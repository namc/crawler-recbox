import utils
import algorithm
import codecs

streamWriter = codecs.lookup('utf-8')[-1]

URL = utils.sanitize_url('http://www.python.org/', '')
MAX_LINK_TO_FETCH = 100
print "URL\tREPEAT-COUNT\tNUMBER-OF-LINKS"
print "\t(numbe of times url repeated)\t(number of links in url web page)"

algorithm.Crawl(URL, MAX_LINK_TO_FETCH)
