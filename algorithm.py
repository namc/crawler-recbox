import requests
import BeautifulSoup
import utils
import codecs

class Crawl(object):
    """docstring for Crawl"""
    def __init__(self, link, link_to_fetch):
        super(Crawl, self).__init__()
        self.url_dict = {}
        self.link_to_fetch = link_to_fetch
        self.base_link = link
        self.depth_first_search(link)

    def depth_first_search(self, link):

        if link in self.url_dict.keys():
            self.url_dict[link]['repeat_count'] = self.url_dict[
                    link]['repeat_count']+1
            print link+"\t" + str(self.url_dict[link]['repeat_count'])+"\t" + \
                str(self.url_dict[link]['number_of_links'])
        else:
            url_links = generate_links(link, self.base_link)
            print link+"\t"+'0'+"\t"+str(len(url_links))

            self.url_dict.update({link: {'repeat_count': 0,
                                 'number_of_links': len(url_links)}})

            for url in url_links:
                if self.link_to_fetch > 0:
                    self.link_to_fetch = self.link_to_fetch - 1
                    self.depth_first_search(url)



streamWriter = codecs.lookup('utf-8')[-1]

def generate_links(link, base_link):
    """docstring for generate_links"""
    a_url_list = []
    try:
        response = requests.get(link)
        dom_tree = BeautifulSoup.BeautifulSoup(response.text)
        a_element_list = dom_tree.fetch('a')  # a elements in html doc
        for a_element in a_element_list:
            a_url = a_element.get('href')
            if utils.is_valid_url(a_url, base_link):
                a_url_list.append(utils.sanitize_url(a_url, base_link))
    except:
        return a_url_list
    return a_url_list
